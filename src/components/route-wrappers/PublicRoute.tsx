import React, { FC, useContext } from "react";
import { Route, RouteProps } from "react-router";
import { Redirect } from "react-router-dom";
import AuthContext from "src/context/auth/auth.context";
import Layout from "src/layout/Layout";

const PublicRoute: FC<RouteProps> = ({ children, ...restProps }) => {
  const { path } = restProps;
  const { authState } = useContext(AuthContext);
  const pathAuth = ["/auth/login", "/auth/registration"];
  const isPathAuth = pathAuth.includes(path as string);

  return (
    <Route
      {...restProps}
      render={({ location }) =>
        !authState?.isAuthenticated ? (
          isPathAuth ? (
            children
          ) : (
            <Layout authState={authState}> {children}</Layout>
          )
        ) : isPathAuth ? (
          <Redirect
            to={{
              pathname: "/home",
              state: { from: location },
            }}
          />
        ) : (
          <Layout> {children}</Layout>
        )
      }
    />
  );
};
export default PublicRoute;
