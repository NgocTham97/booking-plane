interface IRoute {
  page: string;
  exact?: boolean;
  path: string;
}

export const publicRoutes: Readonly<Record<string, IRoute>> = {
  login: {
    page: "login",
    exact: true,
    path: "/auth/login",
  },
  registration: {
    page: "registration",
    exact: true,
    path: "/auth/registration",
  },
  homeId: {
    page: "home/id",
    exact: true,
    path: "/home/:id",
  },
  home: {
    page: "home",
    exact: true,
    path: "/home",
  },
  contact: {
    page: "contact",
    exact: true,
    path: "/contact",
  },
  introduce: {
    page: "introduce",
    exact: true,
    path: "/introduce",
  },
  flightsecsion: {
    page: "flightsection",
    exact: true,
    path: "/flightsection",
  },
  ticketflight: {
    page: "ticketflight",
    exact: true,
    path: "/ticketflight",
  },
  information: {
    page: "information",
    exact: true,
    path: "/information",
  },
  planeseting: {
    page: "planeseting",
    exact: true,
    path: "/planeseting",
  },
  pagepayment: {
    page: "page-payment",
    exact: true,
    path: "/pagepayment",
  },
  revieworder: {
    page: "revieworder",
    exact: true,
    path: "/revieworder",
  },
  
};

export const protectedRoutes: Readonly<Record<string, IRoute>> = {
  users: {
    page: "users",
    exact: true,
    path: "/users",
  },
   admin: {
    page: "admin",
    exact: true,
    path: "/admin",
  },
  manageticketpage: {
    page: "manageticketpage",
    exact: true,
    path: "/manageticketpage",
  },
  usersinformation: {
    page: "usersinformation",
    exact: true,
    path: "/information/users",
  },
  news: {
    page: "news",
    exact: true,
    path: "/news",
  },
};
