import { FacebookFilled,YoutubeOutlined,TwitterOutlined,LinkedinOutlined } from "@ant-design/icons";
import React from "react";
import en from "../assets/images/language/en.svg";
import vi from "../assets/images/language/vi.svg";
import "../style/footer.scss";

const Footer: React.FC = () => {
  return (
    <footer style={{background:"#191c24"}} >
   
    <section
      className="d-flex justify-content-center justify-content-lg-between p-4 border-bottom"
    >
      <div>
        <a href="" className="me-4 text-reset">
          <i className="fab fa-facebook-f"></i>
        </a>
        <a href="" className="me-4 text-reset">
          <i className="fab fa-twitter"></i>
        </a>
        <a href="" className="me-4 text-reset">
          <i className="fab fa-google"></i>
        </a>
        <a href="" className="me-4 text-reset">
          <i className="fab fa-instagram"></i>
        </a>
        <a href="" className="me-4 text-reset">
          <i className="fab fa-linkedin"></i>
        </a>
        <a href="" className="me-4 text-reset">
          <i className="fab fa-github"></i>
        </a>
      </div>
   
    </section>
    <section className="">
      <div style={{color:"white"}} className="container text-left  text-md-start mt-5">
   
        <div className="row mt-3">
          <div className="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
        
            <h6 style={{color:"white"}} className="text-uppercase  fw-bold mb-4">
              Liên hệ
            </h6>
            <p>
              <a href="#!" className="text-reset">Contact@bookingflight.com</a>
            </p>
            <p>
              <a href="#!" className="text-reset">Bookingflight.com</a>
            </p>
            <p>
              <a href="#!" className="text-reset">Phone: 0905019135</a>
            </p>
           
          </div>
      
 
          <div  className="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
        
            <h6 style={{color:"white"}} className="text-uppercase  fw-bold mb-4">
             Về chúng tôi
            </h6>
            <p>
              <a href="#!" className="text-reset">Giới thiệu về Booking flight</a>
            </p>
            <p>
              <a href="#!" className="text-reset">Các đơn vị hợp tác</a>
            </p>
            <p>
              <a href="#!" className="text-reset">Cơ hội hợp tác</a>
            </p>
            <p>
              <a href="#!" className="text-reset">Cấu trúc trang web</a>
            </p>
            <p>
              <a href="#!" className="text-reset">Điều khoảng sử dụng</a>
            </p>
            <p>
              <a href="#!" className="text-reset">Chính sách bảo mật</a>
            </p>
            <p>
              <a href="#!" className="text-reset">Câu hỏi thường gặp</a>
            </p>
          </div>
         
  
     
          <div className="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
       
            <h6 style={{color:"white"}} className="text-uppercase fw-bold mb-4">
              Hưỡng dẫn
            </h6>
            <p><i className="fas fa-home me-3"></i> Hướng dẫn đặt vé máy bay</p>
            <p>
              <i className="fas fa-envelope me-3"></i>
              Hướng dẫn thanh toán
            </p>
            <p><i className="fas fa-phone me-3"></i> Hưỡng dẫn đổi trả vé</p>
          </div>

          <div className="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
          
          <h6 style={{color:"white"}} className="text-uppercase  fw-bold mb-4">
            <i  className="fas fa-gem me-3"></i>Map
          </h6>
          <p>
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3833.984622663514!2d108.23873831422388!3d16.066287743785725!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x314217811efa07b5%3A0x9867d9baeb019e9c!2zODUgQsO5aSBI4buvdSBOZ2jEqWEsIFBoxrDhu5tjIE3hu7ksIFPGoW4gVHLDoCwgxJDDoCBO4bq1bmcgNTUwMDAwLCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1650937982271!5m2!1svi!2s" width="100%" height="200"   loading="lazy"></iframe>
          </p>
        </div>
        </div>
      
      </div>
    </section>
   
  
    
    <div style={{color:"white"}} className="footer-bottom" >
    <a href="#!" className="footer-image"> <FacebookFilled /></a> 
    <a href="#!" className="footer-image"> <YoutubeOutlined /></a> 
    <a href="#!" className="footer-image">  <LinkedinOutlined /></a> 
    <a href="#!" className="footer-image">  <TwitterOutlined /> </a>    
    </div>  
   
  </footer>
  );
};

export default Footer;
