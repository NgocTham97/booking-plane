import React, { useContext } from "react";
import logo from "../assets/images/logo.svg";
import logoMini from "../assets/images/logo-mini.svg";
import { Link } from "react-router-dom";
import { BASE_AUTHENTICATION } from "src/constant";
import AuthContext from "src/context/auth/auth.context";
import { Menu, Dropdown } from "antd";
import { DownOutlined } from "@ant-design/icons";
import avatarNotFound from "../assets/images/faces/avatar-not-found.png";
import "../style/header/header.scss";
import logoplan from "../assets/images/367353.svg";
const Header: React.FC = () => {
  const { authState, dispatchAuthAction } = useContext(AuthContext);
  const logOutHandler = () => {
    localStorage.removeItem(BASE_AUTHENTICATION);
    dispatchAuthAction({
      type: "SWITCH_AUTH_STATE",
      payload: { state: false },
    });
  };
  const menu = (
    <Menu>
      <Menu.Item danger>
        <Link onClick={logOutHandler} to="#" className="nav-link">
          <i className="ti-write menu-icon"></i>
          <span className="menu-title">Đăng xuất</span>
        </Link>
      </Menu.Item>
    </Menu>
  );
  const menu1 = (
    <Menu>
      <Menu.Item danger>
      <Link
              className="header-title"
              to={{
                pathname: "/introduce",
              }}
            >
             Giới thiệu bạn bè
            </Link>
      </Menu.Item>
    </Menu>
  );
  return (
    <div>
      <nav
        style={{ background: "#191c24" }}
        className="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row "
      >
        <div className="logo-header">
          <div className="container">
            <div className="box-logo-header">
              <div className="logo-title">BookingFlight</div>
              <img
                className="logo-image"
                style={{ width: "60px", height: "60px" }}
                src={logoplan}
                alt=""
              />
            </div>
          </div>
        </div>
        <div className="navbar-menu-wrapper d-flex align-items-center justify-content-end">
          <div className="header-title">
          <Link
              className="header-title"
              to={{
                pathname: "/home",
              }}
            >
             Trang chủ
            </Link>
          
            <Link
              className="header-title"
              to={{
                pathname: "/revieworder",
              }}
            >
            Xem lại đơn hàng
            </Link>
            <Link
              className="header-title"
              to={{
                pathname: "/introduce",
              }}
            >
              Giới thiệu
            </Link>
            <Link
              className="header-title"
              to={{
                pathname: "/contact",
              }}
            >
              Liên hệ
            </Link>
          </div>
          {/* search */}
          <ul className="navbar-nav mr-lg-2">
           
            {/* <img
              style={{ width: "50px", height: "50px" }}
              src={avatarNotFound}
              alt=""
            /> */}
          </ul>
          {authState?.isAuthenticated ? (
            <>
              <Dropdown overlay={menu}>
                <a
                  className="ant-dropdown-link"
                  onClick={(e) => e.preventDefault()}
                >
                  Admin <DownOutlined />
                </a>
              </Dropdown>
            </>
          ) : (
            <Link
              className="header-title"
              to={{
                pathname: "/auth/login",
              }}
            >
              Đăng nhập
            </Link>
          )}
        </div>
      </nav>
    </div>
  );
};

export default Header;
