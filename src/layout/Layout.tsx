import React from "react";
import Banner from "./Banner";
import Footer from "./Footer";
import Header from "./Header";
import "./style/index.scss";
// import SidebarLeft from "./SidebarLeft";

const Layout: React.FC<any> = ({ children, ...ref }) => {
  return (
    <div className="container-scroller">
      <Header />
      {/* <div className="container-fluid page-body-wrapper body-wrapper"> */}
        {/* <SidebarLeft /> */}
        {/* <div className="container"> */}
          {/* <Banner /> */}
          <div className="content">{children}</div>
        {/* </div>   */}
      {/* </div> */}
      <Footer />
    </div>
  );
};

export default Layout;
