import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { BASE_AUTHENTICATION } from "src/constant";
import AuthContext from "src/context/auth/auth.context";
import ticket from '../assets/images/icon/ticket-flight.png';
import planmanage from '../assets/images/icon/travel.png';
import exit from '../assets/images/icon/exit_to_app.svg';
import users from '../assets/images/icon/user.png';


const SidebarLeft: React.FC = () => {
  const { dispatchAuthAction } = useContext(AuthContext);
  const logOutHandler = () => {
    localStorage.removeItem(BASE_AUTHENTICATION);
    dispatchAuthAction({
      type: "SWITCH_AUTH_STATE",
      payload: { state: false },
    });
  };

  return (
    <nav className="sidebar sidebar-offcanvas" id="sidebar">
      <ul className="nav">
        <li className="nav-item">
        <Link 
          className="nav-link"
            to={{
              pathname: "/admin",
            }}
          >
              <i className="ti-write menu-icon"></i>
              <img style={{width: '25px' , height: '25px',marginRight: '4px'}} src={planmanage} alt="" />
              <span className="menu-title">Quản lý chuyến bay</span>
          </Link>
        </li>
        <li className="nav-item">
          <Link 
          className="nav-link"
            to={{
              pathname: "/manageticketpage",
            }}
          >
              <i className="ti-write menu-icon"></i>
              <img style={{width: '25px' , height: '25px',marginRight: '4px'}} src={ticket} alt="" />
              <span className="menu-title">Quản lý vé</span>
          </Link>
        </li>
        <li className="nav-item">
          <Link 
          className="nav-link"
            to={{
              pathname: "/users",
            }}
          >
              <i className="ti-write menu-icon"></i>
              <img style={{width: '25px' , height: '25px',marginRight: '4px'}} src={users} alt="" />
              <span className="menu-title">Quản lý Users</span>
          </Link>
        </li>
        <li className="nav-item">
          <Link onClick={logOutHandler} to="#" className="nav-link">
            <i className="ti-write menu-icon"></i>
            <img style={{width: '25px' , height: '25px',marginRight: '4px'}} src={exit} alt="" />
            <span className="menu-title">Đăng xuất</span>
          </Link>
        </li>
      </ul>
    </nav>
  );
};

export default SidebarLeft;
