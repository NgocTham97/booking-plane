import React, { useState, useEffect, useCallback } from "react";
import "./style/index.scss";
import logoplan from "../../assets/images/images.png";
import logo from "../../assets/images/arrow-next.png";
import plan from "../../assets/images/304892.svg";
import { Link, useLocation } from "react-router-dom";
import SidebarLeft from "src/layout/SidebarLeft";
import moment, { Moment } from "moment";
import { Table } from "antd";
import axiosClient from "src/config/call-api";
import noData from '../../../src/assets/images/icon/No-data.svg';
import del from '../../../src/assets/images/icon/delete-table.svg';
import edit from '../../../src/assets/images/icon/create-table.svg';
import see from '../../../src/assets/images/icon/see.svg';
import Text from 'antd/lib/typography/Text';
import Modal from "antd/lib/modal/Modal";
import { Controller, useForm } from "react-hook-form";
import { useYupValidationResolver } from "src/hook";
import { InputForm } from "src/components/form";
import * as yup from "yup";
import { Button } from 'antd';
import { DatePicker } from "antd";
import addbutton from '../../assets/images/icon/add_circle_outline.svg'



const validationSchema = yup.object({
  // username: yup
  //   .string()
  //   .required("Required !")
  //   .trim("The contact name cannot include leading and trailing spaces"),
  // password: yup
  //   .string()
  //   .required("Required !")
  //   .trim("The contact name cannot include leading and trailing spaces"),
});
interface SearchedFlights {
  originCity: any;
  destinationCity: any;
  flights: any;
  flightStatus: any;
  length: any;
  departureTime: any;
  rootPrice: any;
  arrivalTime: any;
  aircraftType: any;
  id: any;

}
const ManageFlight: React.FC = () => {
  const [data, setData] = useState<SearchedFlights>();
  const [flightDetail, setFlightDetail] = useState<SearchedFlights>();
  const [getCity, setGetCity] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [perPage, setPerPage] = useState(1);
  const [page, setPage] = useState(1);
  const [delLs, setDelLs] = useState(false);
  const [importFlight, setImportFlight] = useState(false);
  const [importFlightNote, setImportFlightNote] = useState(false);
  const [delLsNote, setDelLsNote] = useState(false);
  const [flightDetailNote, setFlightDetailNote] = useState(false);
  const [id, setId] = useState(null);
  const dataSource = [];
  const [destinationCities, setDestinationCities] = useState([]);

  const fetchDestinationCities = (originId: any) => {
    setIsLoading(true);
    axiosClient
      .get(`/api/v1/cities/destination?originId=${originId}`)
      .then((response) => {
        console.log(response);
        if (response && response.data) {
          setDestinationCities(response.data);
        }
        setIsLoading(false);
      })
      .catch((e) => {
        setIsLoading(false);
      });
  };
  const resolver = useYupValidationResolver(validationSchema);
  const {
    setError,
    reset,
    register,
    handleSubmit,
    control,
    formState: { errors },
  } = useForm<any>({ resolver, mode: "onChange" });
  const { departureTime, arrivalTime, rootPrice, originAirportId, destinationAirportId, aircraftType } = errors || {};
  for (let i = 0; i < data?.length; i += 1) {
    dataSource.push({
      key: '1',
      status: data?.flightStatus,
      name: 'Mike',
      age: 32,
      address: '10 Downing Street',
    });
  }

  const columns: any = [

    {
      title: 'FlightStatus',
      dataIndex: 'flightStatus',

    },
    {
      title: 'Daystart',
      dataIndex: 'departureTime',
      render: (record: any) => <Text>{moment(record).format("DD/MM/YYYY")}</Text>

    },
    {
      title: 'OriginAirport',
      dataIndex: 'originCity',
      render: (record: any) => <Text>{record?.cityName}</Text>
    },
    {
      title: 'DestinationAirport',
      dataIndex: 'destinationCity',
      render: (record: any) => <Text>{record?.cityName}</Text>
    },
    {
      title: 'RootPrice',
      dataIndex: 'rootPrice',
      render: (record: any) => <Text>{record * 23000}VND</Text>

    },
    {
      title: 'AircraftType',
      dataIndex: 'aircraftType',

    },
    {
      title: 'FlightNumber',
      dataIndex: 'flightNumber',

    },
    {
      title: 'Thao tác',
      width: '10%',
      align: 'left',
      render: (_: any, record: any) => (
        <div className="wrapper-action">
          {/* <div className="wrap-icon">
                  <img src={see} alt='' />
                </div> */}
          <div className="wrap-icon">
            <img
              onClick={() => {
                setFlightDetailNote(true);
                getFlightDetail(record.id);
                setDestinationCities([]);
              }}

              src={edit} alt='' />
          </div>
          <div className="wrap-icon">
            <img onClick={() => {
              setDelLs(true);
              setId(record.id);

            }} src={del} alt='' />
          </div>
        </div>
      ),
    },
  ];
  const DeleteFlight = useCallback((id: any) => {
    setIsLoading(true);
    axiosClient
      .delete(`/api/admin/v1/flights/${id}`)
      .then((response) => {
        if (response && response.data) {
          // setGetCity(response.data);
        }
        setIsLoading(false);

        fetchData();
        setDelLs(false)
        setDelLsNote(true)
      })
      .catch((e) => {
        console.log("error ::", e)
        setIsLoading(false);
      });
  }, []);

  const handleDeleteFlight = (id: any) => {
    //   console.log('id',id);
    DeleteFlight(id);
  }
  const fetchgetCity = useCallback(() => {
    setIsLoading(true);
    axiosClient
      .get(`/api/v1/cities`)
      .then((response) => {
        if (response && response.data) {
          setGetCity(response.data);
        }
        setIsLoading(false);

      })
      .catch((e) => {
        setIsLoading(false);
      });
  }, []);
  const getFlightDetail = useCallback((id: any) => {
    setIsLoading(true);
    axiosClient
      .get(`/api/admin/v1/flights/${id}`)
      .then((response) => {
        if (response && response.data) {
          setFlightDetail(response.data);
          console.log('reponse', response);
        }
        setIsLoading(false);

      })
      .catch((e) => {
        setIsLoading(false);
      });
  }, []);
  const onSubmit = useCallback(
    (data: any) => {
      const { departureTime, arrivalTime, rootPrice, originAirportId, destinationAirportId, aircraftType } = data;
      console.log('data', data)
      setIsLoading(true);
      axiosClient
        .post(`/api/admin/v1/flights`, {
          departureTime: departureTime.format("yyyy-MM-DDTHH:mm:ss"),
          arrivalTime: arrivalTime.format("yyyy-MM-DDTHH:mm:ss"),
          rootPrice,
          originAirportId,
          destinationAirportId,
          aircraftType,
        })
        .then((response) => {
          if (response && response.data) {
           
          }
          setIsLoading(false);
          fetchData();
          setImportFlight(false);
          setImportFlightNote(true);
        })

    },
    []);

  function disabledDate(current: any) {
    // Can not select days before today and today
    const currentDay = moment();
    return current && current.valueOf() < currentDay.valueOf();

  }
  // function range(start: any, end: any) {
  //   const result = [];
  //   for (let i = start; i < end; i++) {
  //     result.push(i);
  //   }
  //   return result;
  // }
  // function disabledDateTime() {

  //   const hour = +(moment().format('HH')) + 1 
  //   console.log('hour', hour + 1)
  //   return {
  //     disabledHours: () => range(0, 24).splice(0, hour),

  //   };
  // }


  const fetchData = useCallback(() => {
    setIsLoading(true);
    axiosClient
      .get(`/api/admin/v1/flights`)
      .then((response) => {
        if (response && response.data) {
          setData(response.data);
        }
        setIsLoading(true);
      })
      .catch((e) => {
      }).finally(() => {
        setIsLoading(false);
      }
      );
  }, []);
  // const editFlight = useCallback(( id: any) => {
    
    
  //   setIsLoading(true);
  //   axiosClient
  //     .put(`/api/admin/v1/flights/${id}`, {
  //       departureTime: departureTime,
  //       arrivalTime: arrivalTime,
  //       rootPrice,
  //       originAirportId,
  //       destinationAirportId,
  //       aircraftType,
  //     })
  //     .then((response) => {
  //       if (response && response.data) {
  //         setData(response.data);
  //       }
  //       setIsLoading(true);
  //     })
  //     .catch((e) => {
  //     }).finally(() => {
  //       setIsLoading(false);
  //     }
  //     );
  // }, []);
  useEffect(() => {
    fetchData();
    fetchgetCity();
  }, [fetchData, fetchgetCity]);


  return (
    <div className="container-fluid page-body-wrapper body-wrapper">

      <div className="box-manage-flight">
        <SidebarLeft />
        <div className="box-manage-flight-content">
          {delLs && (
            <Modal
              onCancel={() => setDelLs(false)}
              visible={delLs}
              title="BẠN CÓ MUỐN XOÁ CHUYẾN BAY KHÔNG?"
              onOk={() => handleDeleteFlight(id)}
            />
          )}
          {importFlight && (
            <Modal
              onCancel={() => setImportFlight(false)}
              visible={importFlight}
              title="BẠN CÓ MUỐN THÊM CHUYẾN BAY KHÔNG?"
              onOk={handleSubmit(onSubmit)}
            >
              <form className="pt-3 needs-validation">
                <div className="form-group" style={{ display: 'grid', gridTemplateColumns: '1fr 1fr', gap: '0 10px' }}>
                  <Controller
                    control={control}
                    name="departureTime"

                    render={(props) => (
                      <DatePicker
                        showTime
                        disabledDate={disabledDate}
                        // disabledTime={disabledDateTime}
                        format={'DD/MM/YYYY HH:mm'}
                        placeholder="Nhập thời gian đi"
                        {...props}
                        onChange={(e: any) => {
                          (props.field).onChange(e);

                        }}
                      />
                    )}
                  />
                  <Controller
                    control={control}
                    name="arrivalTime"

                    render={(props) => (
                      <DatePicker
                        showTime
                        disabledDate={disabledDate}
                        // disabledTime={disabledDateTime}
                        format={'DD/MM/YYYY HH:mm'}
                        placeholder="Nhập thời gian đến"
                        {...props}
                        onChange={(e: any) => {
                          (props.field).onChange(e);

                        }}
                      />
                    )}
                  />
                </div>
                <div className="form-group">

                </div>
                <div className="form-group">
                  <InputForm
                    type="rootPrice"
                    IsInvalid={!!rootPrice}
                    placeholder="Giá tiền"
                    fieldError={rootPrice}
                    data-testid="password-input"
                    {...register("rootPrice")}
                  />

                </div>
                <div className="form-group">
                  {/* <InputForm
                    type="originAirportId"
                    IsInvalid={!!originAirportId}
                    placeholder="Nhập Điểm đi"
                    fieldError={originAirportId}
                    data-testid="originAirportId"
                    {...register("originAirportId")}
                  /> */}
                  <select
                    placeholder="Nhập Điểm đi"
                    className="browser-default custom-select mb-4"
                    {...register("originAirportId")}
                    onChange={(e) => fetchDestinationCities(e.target.value)}
                  >
                    <option value="">Chọn Điểm đi</option>
                    {getCity?.map((city: any, index) => (
                      <option key={index} value={city?.id}>
                        {city?.cityName}
                      </option>
                    ))}
                  </select>
                </div>
                <div className="form-group">
                  {/* <InputForm
                    type="destinationAirportId"
                    IsInvalid={!!destinationAirportId}
                    placeholder="Nhập điểm đến"
                    fieldError={destinationAirportId}
                    data-testid="password-input"
                    {...register("destinationAirportId")}
                  /> */}
                  <select
                    className="browser-default custom-select mb-4"
                    {...register("destinationAirportId")}
                  >
                    <option value="">Chọn Điểm đến</option>
                    {destinationCities?.map((city: any, index) => (
                      <option value={city?.id} key={index}>
                        {city?.cityName}
                      </option>
                    ))}
                  </select>
                </div>
                <div className="form-group">
                  <InputForm
                    type="aircraftType"
                    IsInvalid={!!aircraftType}
                    placeholder="Nhập loại máy bay"
                    fieldError={aircraftType}
                    data-testid="password-input"
                    {...register("aircraftType")}
                  />

                </div>
              </form>
            </Modal>
          )}

          <Modal
            onCancel={() => setFlightDetailNote(false)}
            visible={flightDetailNote}
            title="Sửa chuyến bay?"
            // onOk={()=>editFlight(flightDetail?.id)}
            okText="Cập nhật"
          >
            <form className="pt-3 needs-validation">
              <div className="form-group" style={{ display: 'grid', gridTemplateColumns: '1fr 1fr', gap: '0 10px' }}>
                <Controller
                  control={control}
                  name="departureTime"

                  render={(props) => (
                    <DatePicker
                      showTime
                      disabledDate={disabledDate}
                      value={moment(flightDetail?.departureTime, 'YYYY-MM-DDTHH:mm')
                      }
                      // disabledTime={disabledDateTime}
                      format={'DD/MM/YYYY HH:mm'}
                      placeholder="Nhập thời gian đi"
                      {...props}
                      onChange={(e: any) => {
                        (props.field).onChange(e);

                      }}
                    />
                  )}
                />
                <Controller
                  control={control}
                  name="arrivalTime"

                  render={(props) => (
                    <DatePicker
                      showTime
                      disabledDate={disabledDate}
                      value={moment(flightDetail?.arrivalTime, 'YYYY-MM-DDTHH:mm')
                      }
                      // disabledTime={disabledDateTime}
                      format={'DD/MM/YYYY HH:mm'}
                      placeholder="Nhập thời gian đến"
                      {...props}
                      onChange={(e: any) => {
                        (props.field).onChange(e);

                      }}
                    />
                  )}
                />
              </div>
              <div className="form-group">

              </div>
              <div className="form-group">
                <InputForm
                  type="rootPrice"
                  IsInvalid={!!rootPrice}
                  defaultValue={flightDetail?.rootPrice}
                  placeholder="Giá tiền"
                  fieldError={rootPrice}
                  data-testid="password-input"
                  {...register("rootPrice")}
                />

              </div>
              <div className="form-group">
               
                <select
                  placeholder="Nhập Điểm đi"
                  className="browser-default custom-select mb-4"
                  {...register("originAirportId")}
                  onChange={(e) => fetchDestinationCities(e.target.value)}
                  value={flightDetail?.originCity?.id}
                >
                  <option value="">Chọn Điểm đi</option>
                  {getCity?.map((city: any, index) => (
                    <option key={index} value={city?.id}>
                      {city?.cityName}
                    </option>
                  ))}
                </select>
              </div>
              <div className="form-group">
              
                <select
                  className="browser-default custom-select mb-4"
                  {...register("destinationAirportId")}
                  value={flightDetail?.destinationCity?.id}
                >
                  <option value="">Chọn Điểm đến</option>
                  {destinationCities.length && destinationCities?.map((city: any, index) => (
                    <option value={city?.id} key={index}>
                      {city?.cityName}
                    </option>
                  ))}

                  {!destinationCities.length && getCity?.map((city: any, index) => (
                    <option key={index} value={city?.id}>
                      {city?.cityName}
                    </option>
                  ))}
                </select>
              </div>
              <div className="form-group">
                <InputForm
                  type="aircraftType"
                  defaultValue={flightDetail?.aircraftType}
                  IsInvalid={!!aircraftType}
                  placeholder="Nhập loại máy bay"
                  fieldError={aircraftType}
                  data-testid="password-input"
                  {...register("aircraftType")}
                />

              </div>
            </form>
          </Modal>

          {delLsNote && (
            <Modal
              onCancel={() => setDelLsNote(false)}
              visible={delLsNote}
              title="Xóa thành công"
              cancelButtonProps={{ style: { display: 'none' } } as any}
              onOk={() => setDelLsNote(false)}
            />
          )}
          {importFlightNote && (
            <Modal
              onCancel={() => setImportFlightNote(false)}
              visible={importFlightNote}
              title="Thêm thành công"
              cancelButtonProps={{ style: { display: 'none' } } as any}
              onOk={() => setImportFlightNote(false)}
            />
          )}

          <Button
            style={{ marginBottom: '24px' }}
            onClick={() => { setImportFlight(true) }} >
            <img className="button_add" src={addbutton} alt="" />
            Thêm</Button>
          <Table
            dataSource={data as any}
            columns={columns}
            locale={{
              emptyText: (
                <span>
                  <img src={noData} alt='' />
                </span>
              ),
            }}
          />
        </div>
      </div>
    </div>

  );
};

export default ManageFlight;
