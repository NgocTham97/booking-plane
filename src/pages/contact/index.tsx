import React from "react";
import "./style/index.scss";
import contact from "../../assets/images/contact-us.jpg";
const Contact: React.FC = () => {
  return (
    <div className="container-fluid page-body-wrapper body-wrapper">
    <div className="container">

    
        <div className="box-contact">
          <div className="contact-field
      ">
            {" "}
            <h2 className="title-contact">Liên hệ với chúng tôi</h2>
            <div>Chúng tôi luôn sẵn sàng hỗ trợ, dù bạn ở bất cứ nơi đâu!</div>

          </div>
          <div className="box-contact-us">
            <div className="box-contact-us-left">
              <div className="title">Liên hệ</div>

              <div>Thứ 2 - Chủ nhật (24/7)</div>
              <div> Điện thoại: +84787702293 </div>
              <div> Địa chỉ : 85 Bùi Hữu Nghĩa, Phước Mỹ,Sơn Trà, TP Đà Nẵng </div>
              <div>Để biết thêm thông tin, xin vui lòng liên hệ với chúng tôi qua điện thoại hoặc nhấp vào liên kết bên dưới.</div>

              <button className="btn_contact">Liện hệ với chúng tôi</button>
            </div>
            {/* <div  className="box-contact-us-right">
          <img src={contact} alt="" />
        </div> */}

          </div>
          <div className="box-contact-us">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3833.984622663514!2d108.23873831422388!3d16.066287743785725!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x314217811efa07b5%3A0x9867d9baeb019e9c!2zODUgQsO5aSBI4buvdSBOZ2jEqWEsIFBoxrDhu5tjIE3hu7ksIFPGoW4gVHLDoCwgxJDDoCBO4bq1bmcgNTUwMDAwLCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1650937982271!5m2!1svi!2s" width="100%" height="450"></iframe>
          </div>
        </div>
        </div>
    </div>
  );
};

export default Contact;
