import React, { useEffect, useState } from "react";
import "./style/index.scss";
import logoplan from "../../assets/images/304892.svg";
import { Link, useLocation } from "react-router-dom";
import axiosClient from "src/config/call-api";
import moment from "moment";
interface SearchedFlights {
  originCity: any;
  destinationCity: any;
  flights: any;
  departureTime: any;
  length: any;
}
const FlightSection: React.FC = () => {
  const search = useLocation().search;
  // const state = useLocation().state;

  // const [searchQuery, setSearchQuery] = useState(state);
  const [data, setData] = useState<SearchedFlights>();
  const [flightKey, setFlightKey] = useState(2);
  const [selectSeat, setSelectSeat] = useState<SearchedFlights>();
  const [detail, setDetail] = useState<any>({});

  console.log('detail', detail)
  useEffect(() => {
    getData(search);
  }, [search]);
  const handledetail = (id: any) => {

    const exits = detail?.[id]

    if (exits) {
      return setDetail({ ...detail, [id]: !detail?.[id] })
    }
    
    setDetail({ ...detail, [id]: true })

    //   if(detail === false )
    //  {setDetail(true);} 
    //  else
    //  return setDetail(false)
  }
  const getData = (queries: any) => {
    axiosClient
      .get("/api/v1/flights" + queries)
      .then((response) => {
        if (response && response.data) {
          setData(response.data);
        }
      })
      .catch(() => { });
  };

  const getSelect = (id: any) => {
    axiosClient
      .get("/api/v1/flights/selected-tickets?flightId=" + id)
      .then((response) => {
        if (response && response.data) {
          setSelectSeat(response.data);
        }

      })
      .catch(() => { });
  };

  const prices = [
    {
      time: moment(data?.flights[0]?.departureTime)
        .subtract(2, "d")
        .format("DD/MM/YYYY"),
      price: "10000000",
    },
    {
      time: moment(data?.flights[0]?.departureTime)
        .subtract(1, "d")
        .format("DD/MM/YYYY"),
      price: "10000000",
    },
    {
      time: moment(data?.flights[0]?.departureTime).format("DD/MM/YYYY"),
      price: "10000000",
    },
    {
      time: moment(data?.flights[0]?.departureTime)
        .add(1, "d")
        .format("DD/MM/YYYY"),
      price: "10000000",
    },
    {
      time: moment(data?.flights[0]?.departureTime)
        .add(2, "d")
        .format("DD/MM/YYYY"),
      price: "10000000",
    },
  ];
  return (
    <div className="container-fluid page-body-wrapper body-wrapper">
      <div className="container">
        <div className="nav-flight-section">
          <div className="content-flight-section">
            <h2 className="title-content">Chọn Chuyến bay</h2>
            <p className="title-p">
              {" "}
              Chọn chuyến bay {data?.originCity?.cityName} (
              {data?.originCity.shortCode}), Việt Nam -{" "}
              {data?.destinationCity?.cityName} (
              {data?.destinationCity?.shortCode}), Việt Nam
            </p>
          </div>
          <div className="note-box">
            <div className="title">
              Tra cứu thông tin hành lý<a href="">tại đây</a>
            </div>
            <div style={{ color: "red" }} className="title">
              Lưu ý: Giá dưới đây đã bao gồm thuế, phí
            </div>
            <div style={{ color: "blue", cursor: "pointer" }} className="title">
              + Phí Dịch Vụ Đặc Biệt
            </div>
            <div style={{ color: "blue", cursor: "pointer" }} className="title">
              + Thuế, Phí, Lệ phí & Phụ thu
            </div>
            <div className="title">
              Đồng tiền thanh toán hiển thị theo “Quốc gia/Vùng” đã chọn, Quý
              khách kiểm tra kỹ đồng tiền trước khi thanh toán.
            </div>
          </div>
          <div className="content-flight-section">
            <h2 className="title-content">Chuyến bay chiều đi</h2>
            <div className="box-section">
              {prices.map((item, index) => (
                <div
                  onClick={() => setFlightKey(index)}
                  style={{
                    background: index === flightKey && '#005f6e' as any,
                    color: index === flightKey && '#fff' as any,
                  }}
                  className="box-section-in">
                  <div
                    style={{
                      color: index === flightKey && '#fff' as any,
                    }} className="time-box-section">{item.time}</div>
                  {/* <div className="price-box-section">{item.price}</div> */}
                </div>
              ))}
            </div>
          </div>
          <div className="box-filter">
            <div className="box-filter-left">
              <div className="result">{data?.flights.length} kết quả</div>
              <div className="title-filter">Sắp xếp theo:</div>
              <div className="title-filter"> Hiện thị bộ lọc</div>
            </div>
            {/* <div className="box-filter-right">
          <button className="bnt-section">phổ thông</button>
          <button className="bnt-section-1">thương gia</button>
        </div> */}
          </div>
          <div className="time-and">
            <div className="time">Thời gian</div>
            <div className="title">
              Giá bằng VND và MỘT CHIỀU (theo hành khách).
            </div>
          </div>
          <div className="nav-box-content-section">
            {data?.flights.length === 0 ? (
              <div>
                {" "}
                <h2 style={{ textAlign: "center" }}>
                  Không có chuyến bay cho ngày hôm nay
                </h2>
              </div>
            ) : (
              <div>
                {data?.flights?.map((item: any, index: any) => (

                  < div >
                    <div className="box-content-section">
                      <div className="box-content-section-left">
                        <div className="journeys">
                          <div className="go">
                            <div className="time-go">
                              {moment(item?.departureTime).format("HH:mm")}{" "}
                            </div>
                            <div className="dress">
                              {data?.originCity?.cityName}
                            </div>
                          </div>
                          <img
                            src={logoplan}
                            style={{ width: "30px", height: "30px" }}
                            alt=""
                          />
                          <div className="desnitation">
                            <div className="time-go">
                              {moment(item?.arrivalTime).format("HH:mm")}
                            </div>
                            <div className="dress">
                              {data?.destinationCity?.cityName}
                            </div>
                          </div>
                        </div>
                        <div className="time-all">
                          {/* <div className="time-a"></div>{" "} */}
                          <p>Bay Thẳng</p>{" "}
                        </div>
                        <div
                          onClick={() => handledetail(item.id)}
                          className="detail-button"> Xem chi tiết chuyến bay</div>
                      </div>
                      <div className="box-content-section-center">
                        <div className="name-plane">VN{item?.flightNumber}</div>
                        <div className="name-plane"> {item?.aircraftType}</div>
                      </div>
                      <div className="box-content-section-right">
                        <div className="common">
                          <div className="title">Từ</div>
                          <div className="price">
                            {item?.rootPrice * 23000} VND
                          </div>
                          <div className="side-chair" >
                            {/* <div>Còn {48 - selectSeat?.length} Ghế</div> */}
                            <Link
                              className="bnt-reservations"

                              to={{
                                pathname: "/ticketflight",
                                search: `?id=${item?.id}`,
                              }}
                            >
                              Chọn vé
                            </Link>
                          </div>
                        </div>
                      </div>

                    </div>
                    {
                      detail?.[item.id] &&
                      (
                        <div className="detail">
                          <div className="content-top-detail">
                            <div className="date">{moment(item?.departureTime).format("DD/MM/YYYY")},</div>
                            <div className="time"> {moment(item?.departureTime).format("HH:mm")}</div>
                            <div className="city">
                              {data?.originCity?.cityName}
                            </div>
                          </div>
                          <div className="airlinecompany">
                            <div className="air"> Chuyến bay:</div>
                            <div> VN{item?.flightNumber}</div>
                          </div>
                          <div className="airtype">
                            <div className="air">Máy Bay:</div>
                            <div> {item?.aircraftType}</div>

                          </div>
                          {/* <div className="time-go">

                            <div className="air">  Thời gian di chuyển :</div>
                            <div></div>
                          </div> */}
                          <div className="content-top-detail">
                            <div className="date">{moment(item?.arrivalTime).format("DD/MM/YYYY")},</div>
                            <div className="time"> {moment(item?.arrivalTime).format("HH:mm")}</div>
                            <div className="city">
                              {data?.destinationCity?.cityName}
                            </div>
                          </div>
                        </div>)
                    }
                  </div>
                ))}
              </div>
            )}
          </div>
        </div>
      </div>
    </div >
  );
};
export default FlightSection;
