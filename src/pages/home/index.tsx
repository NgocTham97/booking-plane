import React, { useCallback, useEffect, useState } from "react";
import "./style/index.scss";
import downarrow from "../../assets/images/icon/chevron-down.svg";
import danang from "../../assets/images/toan-canh-cau-rong-vo-cung-hoanh-trang.png";
import vhl from "../../assets/images/03c42c8224df159694b52f24a2c323b7.png";
import Slider from "react-slick";
import newsimage1 from "../../assets/images/checking-online.jpg";
import { Link, useHistory } from "react-router-dom";
import { DatePicker } from "antd";
import axiosClient from "src/config/call-api";
import useYupValidationResolver from "src/hook/useYupValidationResolver";
import * as yup from "yup";
import { useForm } from "react-hook-form";
import moment from "moment";

const validationSchema = yup.object({});

const Home = () => {
  const [data, setData] = useState([]);
  const [destinationCities, setDestinationCities] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const resolver = useYupValidationResolver(validationSchema);
  const {
    setError,
    reset,
    register,
    handleSubmit,
    // formState: { errors },
  } = useForm<any>({ resolver, mode: "onChange" });

  const settings = {
    dots: false,
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
  };
  const [hide, setHide] = useState(false);
  const history = useHistory();
  const handleOnChange = () => {
    setHide(true);
  };
  const handleOnChangeOne = () => {
    setHide(false);
  };
  const list = [
    {
      content: "Hà Nội đến TP Đà Nẵng",
      time_start: "Khởi hành: 09/05/2022",
      price: "1000000VND",
      time_watched: "Đã xem: 11hr",
      status: "Một chiều/Phổ Thông",
    },
    {
      content: "Hà Nội đến TP Đà Nẵng",
      time_start: "Khởi hành: 09/05/2022",
      price: "1000000VND",
      time_watched: "Đã xem: 11hr",
      status: "Một chiều/Phổ Thông",
    },
    {
      content: "Hà Nội đến TP Đà Nẵng",
      time_start: "Khởi hành: 09/05/2022",
      price: "1000000VND",
      time_watched: "Đã xem: 11hr",
      status: "Một chiều/Phổ Thông",
    },
    {
      content: "Hà Nội đến TP Đà Nẵng",
      time_start: "Khởi hành: 09/05/2022",
      price: "1000000VND",
      time_watched: "Đã xem: 11hr",
      status: "Một chiều/Phổ Thông",
    },
    {
      content: "Hà Nội đến TP Đà Nẵng",
      time_start: "Khởi hành: 09/05/2022",
      price: "1000000VND",
      time_watched: "Đã xem: 11hr",
      status: "Một chiều/Phổ Thông",
    },
    {
      content: "Hà Nội đến TP Đà Nẵng",
      time_start: "Khởi hành: 09/05/2022",
      price: "1000000VND",
      time_watched: "Đã xem: 11hr",
      status: "Một chiều/Phổ Thông",
    },
    {
      content: "Hà Nội đến TP Đà Nẵng",
      time_start: "Khởi hành: 09/05/2022",
      price: "1000000VND",
      time_watched: "Đã xem: 11hr",
      status: "Một chiều/Phổ Thông",
    },
  ];
  const arr = [
    {
      content: "Đà Nẵng- Thành Phố Đáng Sống",
    },
    {
      content: "Hà Nội- Thủ Đô nghìn năm văn hiến",
    },
    {
      content: "Thành phố HCM- Đô Thị sôi động",
    },
    {
      content: "Thành phố HCM- Đô Thị sôi động",
    },
  ];
  const news = [
    {
      content: "Check-in Online",
      time: "12/05/2022",
    },
    {
      content: "Dịch vụ trên không",
      time: "07/06/2022",
    },
    {
      content: "Ưu đãi đặc biệt cho chủ thẻ tín dụng VIB",
      time: "12/05/2022",
    },
    {
      content: "Ưu đãi mới nhất",
      time: "12/05/2022",
    },
  ];
  const fetchData = useCallback(() => {
    setIsLoading(true);
    axiosClient
      .get(`/api/v1/cities`)
      .then((response) => {
        if (response && response.data) {
          setData(response.data);
        }
        setIsLoading(false);
      })
      .catch((e) => {
        setIsLoading(false);
      });
  }, []);
  const fetchDestinationCities = (originId: any) => {
    setIsLoading(true);
    axiosClient
      .get(`/api/v1/cities/destination?originId=${originId}`)
      .then((response) => {
        console.log(response);
        if (response && response.data) {
          setDestinationCities(response.data);
        }
        setIsLoading(false);
      })
      .catch((e) => {
        setIsLoading(false);
      });
  };

  const onSubmit = (data: any) => {
    const adaper: Record<string, string> = {
      children: "children",
      depart: "destinationId",
      from: "originId",
      gender: "gender",
      oldPerson: "oldPerson",
      timeStart: "departureTime",
      person: "person",
    };

    const dataAdaper: Record<string, string | null> = Object.keys(
      adaper
    ).reduce((acc, key) => ({ ...acc, [adaper[key]]: data[key] }), {});

    Object.keys(dataAdaper).forEach((key) => {
      if (!dataAdaper[key]) {
        delete dataAdaper[key];
      }
      if (adaper["timeStart"] === key) {
        dataAdaper[adaper["timeStart"]] = moment(
          dataAdaper[adaper["timeStart"]]
        ).format("yyyy-MM-DDTHH:mm:ss");
      }
    });

    const newParam = new URLSearchParams(dataAdaper as any).toString();
    history.push({
      pathname: "/flightsection",
      search: "?" + newParam,
      state: dataAdaper,
    });
  };

  useEffect(() => {
    fetchData();
  }, [fetchData]);

  return (
    <div className="container-fluid page-body-wrapper body-wrapper">
      <div className="container">
        <form onSubmit={handleSubmit(onSubmit)}>
          <div
            style={{ marginTop: "3rem" }}
            className="card shadow mb-5 bg-white rounded"
          >
            <div className="card-body">
              <p className="card-title text-center shadow mb-5 rounded">
                BookingFlight Form
              </p>

              <p className="searchText">
                <strong>Tìm kiếm các chuyến bay giá rẻ</strong>
              </p>

              <div className="row mb-3 mt-3">
                <label className="radiobtn">
                  <input
                    type="radio"
                    value="male"
                    {...register("gender")}
                    onChange={() => handleOnChangeOne()}
                  />
                  Một chiều
                </label>
                <label className="radiobtn">
                  <input
                    type="radio"
                    value="male"
                    {...register("gender")}
                    onChange={() => handleOnChange()}
                  />
                  Khứ hồi
                </label>
              </div>

              <div className="row">
                <div className="col-sm-6">
                  <select
                    className="browser-default custom-select mb-4"
                    {...register("from")}
                    onChange={(e) => fetchDestinationCities(e.target.value)}
                  >
                    <option value="">Chọn Điểm đi</option>
                    {data.map((city: any, index) => (
                      <option key={index} value={city?.id}>
                        {city?.cityName}
                      </option>
                    ))}
                  </select>
                </div>
                <div className="col-sm-6">
                  <select
                    className="browser-default custom-select mb-4"
                    {...register("depart")}
                  >
                    <option value="">Chọn Điểm đến</option>
                    {destinationCities.map((city: any, index) => (
                      <option value={city?.id} key={index}>
                        {city?.cityName}
                      </option>
                    ))}
                  </select>
                </div>
              </div>

              <div className="row">
                <div className="col-sm-6">
                  <DatePicker
                    {...register("timeStart")}
                    placeholder=" Ngày đi"
                    className="form-control-date "
                  />
                </div>
                {hide && (
                  <div className="col-sm-6">
                    <DatePicker
                      {...register("timeReturn")}
                      id="datedes"
                      placeholder=" Ngày về"
                      className="form-control-date "
                    />
                  </div>
                )}
              </div>

              {/* <div className="row mt-4">
            <div className="col-sm-6"> <select className="browser-default custom-select mb-4" id="select">
                    <option value="" >Anytime</option>
                    <option value="1">6:00 AM</option>
                    <option value="2">3:00 PM</option>
                    <option value="3">6:00 PM</option>
                </select> </div>
            <div className="col-sm-6"> <select className="browser-default custom-select mb-4" id="select">
                    <option value="" >Anytime</option>
                    <option value="1">6:00 AM</option>
                    <option value="2">3:00 PM</option>
                    <option value="3">6:00 PM</option>
                </select> </div>
        </div> */}
              <div style={{ marginTop: "3rem" }} className="row">
                <div className="col-sm-4">
                  <select
                    className="browser-default custom-select mb-4"
                    {...register("children")}
                  >
                    <option value="">Trẻ em(0-14)</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                  </select>
                </div>
                <div className="col-sm-4">
                  <select
                    className="browser-default custom-select mb-4"
                    {...register("person")}
                  >
                    <option value="">Người lớn(15-64)</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                  </select>
                </div>
                <div className="col-sm-4">
                  <select
                    className="browser-default custom-select mb-4"
                    {...register("oldPerson")}
                  >
                    <option value="">Trẻ sơ sinh</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                  </select>
                </div>
              </div>
              <button
                className="btn btn-primary float-right mt-5"
                // to={{
                //   pathname: "/flightsection",
                // }}
                type="submit"
              >
                Tìm chuyển bay
              </button>
            </div>
          </div>
        </form>

        <div className="nav-box-home">
          <div className="search-title">Tìm kiếm chuyến bay được ưa thích</div>
          <div className="box-search">
            <div className="box-search-from">
              <div className="box-search-from-content">Từ</div>
              <div className="input-search">
                <input
                  className="form-control"
                  placeholder="Nhập điểm đi"
                ></input>
                <img className="down-arrow" src={downarrow} alt="" />
              </div>
            </div>
            <div className="box-search-from">
              <div className="box-search-from-content">Đến</div>
              <div className="input-search">
                <input
                  className="form-control"
                  placeholder="Nhập điểm đến"
                ></input>
                <img className="down-arrow" src={downarrow} alt="" />
              </div>
            </div>
            <div className="box-search-budget">
              <div className="box-search-from-content">Ngân Sách</div>
              <input
                className="form-control"
                placeholder="Nhập Ngân sách tối đa"
              ></input>
            </div>
          </div>
          <div className="nav-box-list">
            {list.map((item, index) => (
              <div className="box-list" key={index}>
                <div className="list">
                  <img className="image-list" src={danang} alt="" />
                  <div className="box-content-list">
                    <div className="top">
                      <div className="content-list">{item.content}</div>
                      <div className="time-start">{item.time_start}</div>
                    </div>
                    <div className="bot">
                      <div className="for">Từ</div>
                      <div className="price">{item.price}</div>
                      <div className="time-watched">{item.time_watched}</div>
                      <div className="status">{item.status}</div>
                    </div>
                    <button className="buy-now">Mua Ngay</button>
                  </div>
                </div>
              </div>
            ))}
          </div>
          <div className="box-bnt-see">
            <button className="bnt-see">Xem thêm</button>
          </div>

          <div className="nav-box-outstanding">
            <div className="content-title">Các điểm đến nổi bật ở Việt Nam</div>
          </div>

          <div className="list-box-outstanding">
            <Slider {...settings}>
              {arr.map((item, index) => (
                <div className="box-outstanding">
                  <img
                    style={{
                      width: "100%",
                      height: "500px",
                      objectFit: "cover",
                    }}
                    src={danang}
                    alt=""
                  />
                  <div className="box-title">
                    <div className="title">{item.content}</div>
                    <button className="bnt-discover">Khám Phá</button>
                  </div>
                </div>
              ))}
            </Slider>
          </div>

          <div className="box-bnt-see">
            <button className="bnt-see">Xem tất cả các điểm đến</button>
          </div>
          <div className="nav-box-news">
            <div className="content-news">Tin tức mới nhất</div>

            <div className="list-box-news">
              <Slider {...settings}>
                {news.map((item, index) => (
                  <div className="box-news">
                    <img
                      src={newsimage1}
                      alt=""
                      style={{ width: "100%", objectFit: "cover" }}
                    />
                    <div className="box-conent-news">
                      <div className="content-news-in">{item.content}</div>
                      <div className="time-news">{item.time}</div>
                    </div>
                  </div>
                ))}
              </Slider>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;
