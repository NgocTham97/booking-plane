import React, { useEffect, useState, useCallback } from "react";
import "./style/index.scss";
import logoplan from "../../assets/images/images.png";
import logo from "../../assets/images/arrow-next.png";
import plan from "../../assets/images/304892.svg";
import { Link, useLocation } from "react-router-dom";
import { useHistory } from "react-router-dom";
import axiosClient from "src/config/call-api";
import { Controller, useForm } from "react-hook-form";
import { useYupValidationResolver } from "src/hook";
import * as yup from "yup";
import moment from "moment";
import { InputForm } from "src/components/form";
import { DatePicker } from "antd";

interface SearchedFlights {
  id: any;
  departureTime: any;
  rootPrice: any;
  flightNumber: any;
  aircraftType: any;
  arrivalTime: any;
  shortCode: any;
  cityName: any;
}
const validationSchema = yup.object({
  contactPhoneNumber: yup
    .string()
    .required("Required !")
    .trim("The contact name cannot include leading and trailing spaces"),
  contactEmail: yup
    .string()
    .required("Required !")
    .trim("The contact name cannot include leading and trailing spaces"),
  middleAndFirstName: yup
    .string()
    .required("Required !")
    .trim("The contact name cannot include leading and trailing spaces"),
    familyName: yup
    .string()
    .required("Required !")
    .trim("The contact name cannot include leading and trailing spaces"),
});

const Information: React.FC = () => {

  const resolver = useYupValidationResolver(validationSchema);
  const {
    setError,
    reset,
    register,
    handleSubmit,
    control,
    formState: { errors },
  } = useForm<any>({ resolver, mode: "onChange" });
  const history = useHistory();
  const search = window.location.search;
  const [data, setData] = useState<SearchedFlights>();
  const [isLoading, setIsLoading] = useState(false);
  const [getCity, setGetCity] = useState<SearchedFlights[] | []>([]);
  const params = new URLSearchParams(search);
  const { contactPhoneNumber, contactEmail, middleAndFirstName ,familyName} = errors || {};
  const id = params.get('id');
  useEffect(() => {
    getData(id);

  }, [id]);
  const fetchgetCity = useCallback(() => {
    setIsLoading(true);
    axiosClient
      .get(`/api/v1/cities`)
      .then((response) => {
        if (response && response.data) {
          setGetCity(response.data);
        }
        setIsLoading(false);
      })
      .catch((e) => {
        setIsLoading(false);
      });
  }, []);
  useEffect(() => {

    fetchgetCity();
  }, [fetchgetCity]);
  const getData = (id: any) => {
    axiosClient
      .get("/api/v1/flights/" + id)
      .then((response) => {
        if (response && response.data) {
          setData(response.data);
        }
      })
      .catch(() => { });
  };
  const onSubmit = (_data: any) => {
    console.log('_data', _data)
    const newParam = new URLSearchParams({ ..._data, id: id } as any).toString();
    history.push({
      pathname: "/planeseting",
      search: "?" + newParam,
    });
    localStorage.setItem('info', JSON.stringify({ ..._data, flightId: id }))
  };

  const dressShow = (id: any) => {
    const curentCity = (getCity).find((city) => city.id === id)
    return curentCity ? curentCity?.cityName : ''
  }
  return (
    <div className="container-fluid page-body-wrapper body-wrapper">
      <div className="container">
        <div className="nav-all">
          <div className="note-box">
            <div className="title">
              Dữ liệu cá nhân của Quý khách thu thập trên trang này được xử lý
              và lưu trữ bởi BookingFlight cho mục đích và theo điều kiện đã
              được công bố tại Chính sách bảo mật thông tin của BookingFlight.
            </div>
            <div className="title">
              Để tìm hiểu thêm về việc cách thức xử lý dữ liệu cá nhân của Quý
              khách và về các quyền của Quý khách (Quyền yêu cầu cung cấp thông
              tin, Quyền sửa đổi thông tin…), vui lòng đọc và chấp nhận Chính
              sách bảo mật thông tin của chúng tôi.
            </div>
          </div>
          <div className="box-title-ticket-flight">
            <div className="title-ticket-flight">Thông tin Hành khách</div>
            <div className="title">
              Quý Khách vui lòng sử dụng tiếng Việt không dấu và không sử dụng
              các ký tự đặc biệt. Vui lòng nhập đầy đủ tên hành khách và những
              thông tin khác xuất hiện trên (các) giấy tờ tùy thân do chính phủ
              cấp của hành khách.
            </div>
          </div>
          <div className="nav-ticket-flight">
            <form onSubmit={handleSubmit(onSubmit)} className="nav-left">
              <div className="note-box">
                <div className="title-content">Thông tin liên hệ</div>
                <div className="box-form">
                  <div>
                    <InputForm
                      //  className="form"
                      IsInvalid={!!contactPhoneNumber}
                      placeholder="Số điện thoại *"
                      fieldError={contactPhoneNumber}
                      // data-testid="username-input"
                      {...register("contactPhoneNumber")}
                    />
                  </div>
                  {/* <input
                    className="form"
                    id="exampleFormControlInput1"
                    placeholder="Số điện thoại *"
                    {...register("contactPhoneNumber")}
                  /> */}
                  <input
                    type="email"
                    className="form"
                    id="exampleFormControlInput1"
                    placeholder="Số điện thoại 2"
                  />
                  <div>
                    <InputForm
                      //  className="form"
                      IsInvalid={!!contactEmail}
                      placeholder="Địa chỉ email *"
                      fieldError={contactEmail}
                      // data-testid="username-input"
                      {...register("contactEmail")}
                    />
                  </div>
                  {/* <input
                    type="email"
                    className="form"
                    id="exampleFormControlInput1"
                    placeholder="Địa chỉ email *"
                    {...register("contactEmail")}
                  /> */}
                  <input
                    type="email"
                    className="form"
                    id="exampleFormControlInput1"
                    placeholder="Địa chỉ email 2"
                  />
                </div>
              </div>
              <div className="note-box">
                <div className="title-content">Thông tin cơ bản</div>
                <div className="box-form-person">
                  <select {...register('prefixOfName')} className="form" id="select">
                    <option value="">Danh Xưng*</option>
                    <option value="Ông">Ông</option>
                    <option value="Bà">Bà</option>
                    <option value="Cô/ Chị">Cô/ Chị</option>
                  </select>
                  <div>
                    <InputForm
                      //  className="form"
                      IsInvalid={!!middleAndFirstName}
                      placeholder="Tên đệm và tên*"
                      fieldError={middleAndFirstName}
                      // data-testid="username-input"
                      {...register("middleAndFirstName")}
                    />
                  </div>
                  {/* <input
                    className="form"
                    id="exampleFormControlInput1"
                    placeholder="Tên đệm và tên*"
                    {...register("middleAndFirstName")}
                  /> */}
{/* 
                  <input
                    className="form"
                    id="exampleFormControlInput1"
                    placeholder="Họ*"
                    {...register("familyName")}
                  /> */}
                  <div>
                    <InputForm
                      //  className="form"
                      IsInvalid={!!familyName}
                      placeholder="Họ*"
                      fieldError={familyName}
                      // data-testid="username-input"
                      {...register("familyName")}
                    />
                  </div>
                </div>
                <div className="box-form">
                  <div className="date-time">
                    <DatePicker
                      format={'YYYY-MM-DD'}
                      placeholder="Nhập ngày sinh"
                      {...register("dateOfBirth")}
                    />
                  </div>
                  <select {...register('gender')} placeholder="Giới tính*" className="form" id="select">
                    <option value="0">Giới tính*</option>
                    <option value="Male">Nam </option>
                    <option value="Female">Nữ</option>
                  </select>
                </div>
              </div>
              <button className="bnt-continue" type="submit">

                Tiếp tục

              </button>
            </form>

            <div className="nav-right">
              <div className="note-box-right">
                <div className="box-content">
                  <p className="title-content">Chi tiết chuyến bay</p>
                </div>
                <div className="flight-section">
                  <div className="flight-section-left">
                    <img
                      src={logoplan}
                      style={{ width: "30px", height: "30px" }}
                      alt=""
                    />

                    <div className="city">HAN</div>
                    <img
                      src={logo}
                      style={{ width: "30px", height: "15px" }}
                      alt=""
                    />
                    <div className="city">DAD</div>
                  </div>
                  <div className="flight-section-right">{data?.rootPrice * 23000}VND</div>
                </div>
                <div className="flight-section-note">
                  <div className="time-start">
                    <div className="title">Khởi hành</div>
                    <div> {moment(data?.departureTime).format("HH:mm")} </div>
                    <div>{moment(data?.departureTime)

                      .format("DD/MM/YYYY")}</div>
                  </div>
                  <div className="flight-section-detail">
                    <div className="flight">
                      <div> {dressShow((data as any)?.originAirportId)}</div>
                      <img
                        src={plan}
                        style={{ width: "15px", height: "15px" }}
                        alt=""
                      />
                      <div> {dressShow((data as any)?.destinationAirportId)}</div>
                    </div>
                    {/* <div className="time">
                      <div>Thời gian:</div>
                      <div>1 tiếng 25 phút</div>
                    </div> */}
                    <div className="plane">
                      <div>VN{data?.flightNumber}</div>
                      <div>{data?.aircraftType}</div>
                    </div>
                  </div>
                  <div className="person">
                    <div className="title">Người lớn x 1 </div>
                    <div className="title">{data?.rootPrice * 23000}VND</div>
                  </div>
                </div>
                <div className="price">
                  <div className="price-all">Tổng số tiền</div>
                  <div className="price-in">{data?.rootPrice * 23000}VND</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Information;
