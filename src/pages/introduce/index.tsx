import React from "react";
import "./style/index.scss";

import { Link } from "react-router-dom";
const Introduce: React.FC = () => {
    return (
        <div className="container-fluid page-body-wrapper body-wrapper">
            <div className="container">
                <div className="box-introduce">
                    <div className="box-content-introduce">
                        <h2 className="title-content">Giới thiệu bạn bè</h2>
                        <h2 className="title-content">Thêm bạn thêm vui</h2>
                    </div>
                    <div className="content-title-introduce">1. THÔNG TIN CHƯƠNG TRÌNH</div>
                    <div className="content-in">Với mong muốn đem lại nhiều lợi ích cho khách hàng, BookingFlight trân trọng giới thiệu chương trình Giới thiệu bạn bè với nhiều giải thưởng hấp dẫn và cách thức tham gia vô cùng đơn giản. Những khách hàng tích cực nhất sẽ có cơ hội nhận được những đêm nghỉ miễn phí từ BookingFlight.</div>
                    <div className="content-title-introduce">2. Lợi ích đến từ chương trình</div>
                    <div className="content-in">Có 6 lợi ích đến từ giới thiệu bạn bè:</div>
                    <div className="people">Người giới thiệu: </div>
                    <div>-10% Giá trị đơn phòng của bạn bè được chuyển vào tài khoản Vpoint</div>
                    <div>-VPOINT Có thể tích lũy để đặt vé miễn phí tại BookingFlight</div>
                    <div>-MÃ GTBB Có thể dùng để giới thiệu nhiều bạn bè cùng một lúc</div>
                    <div className="people">Người được giới thiệu:</div>
                    <div>-100.000 VPOINT Được nhận ngay khi đăng ký tài khoản BookingFlight thành công</div>
                    <div>-VPOINT Nhận được khi đăng kí tài khoản và có thể sử dụng ngay</div>
                    <div>-MÃ GTBB Có thể tiếp tục giới thiệu bạn bè đặt vé tại BookingFlight với mã GTBB của riêng mình</div>
                    <div className="content-title-introduce">3. Cách thức tham gia</div>
                    <div>Mỗi tài khoản tại BookingFlight  có 1 mã Giới thiệu bạn bè (GTBB) (kiểm tra trong phần Quản lý tài khoản)</div>
                    <div>Khi khách hàng A (đã có tài khoản BookingFlight ) giới thiệu cho bạn bè (khách hàng B) tới BookingFlight.vn. Khách hàng B đăng kí tài khoản BookingFlight  thành công với mã Giới thiệu bạn bè của khách hàng A sẽ nhận ngay 100,000 Vpoint (tương đương 100,000đ). Số Vpoint này có thể được sử dụng ngay để giảm trừ trong đơn phòng đầu tiên của khách hàng B.
                        (Lưu ý: Đơn phòng của khách hàng B phải có giá trị tối thiểu 800,000đ thì mới có thể sử dụng giảm trừ 100,000 Vpoint trên từ BookingFlight )</div>
                    <div>Khi khách hàng B đặt và trả phòng thành công, khách hàng A sẽ nhận ngay 10% giá trị đơn phòng của khách hàng B vào tài khoản Vpoint.</div>
                    <div>Khách hàng B có thể tiếp tục chia sẻ mã Giới thiệu bạn bè để tích lũy Vpoint sử dụng trong những chuyến du lịch sau.</div>
                    <div className="content-title-introduce">4. ĐIỀU KIỆN & ĐIỀU KHOẢN</div>
                    <div>- Mã GTBB chỉ được sử dụng cho lần đặt vé đầu tiên mỗi khách hàng mới được giới thiệu khi đặt vé tại BookingFlight .vn.</div>
                    <div>-Khách hàng mới là những khách hàng chưa từng đặt vé trên BookingFlight.vn</div>
                    <div>- Khách hàng mới đăng kí tài khoản bằng cách click vào link được gửi trong email hoặc SMS là tài khoản sẽ được tính là tham gia chương trình Giới thiệu bạn bè</div>
                    <div>- Email và số điện thoại đăng kí tài khoản phải là email và số điện thoại chưa từng có đơn phòng thành công tại BookingFlight </div>
                    <div>- Khách hàng giới thiệu bạn bè có thể đăng tải Mã GTBB hoặc link GTBB của mình trên các phương tiện truyền thông.</div>
                    <div>- Nếu phát hiện gian lận, điểm thưởng từ chương trình sẽ tự động hủy mà không cần thông báo trước. BookingFlight  nghiêm cấm các trường hợp cố tình tách đơn phòng để hưởng khuyến mãi nhiều lần. Đối với các tài khoản vi phạm quy định này, BookingFlight  sẽ ngay lập tức phong tỏa tài khoản điểm thưởng</div>
                    <div>- Trong mọi trường hợp, quyết định của BookingFlight  là quyết định cuối cùng</div>
                    <div>- Chương trình không áp dụng với tài khoản đại lý, khách hàng doanh nghiệp và nhân viên của BookingFlight </div>
                </div>
            </div>

        </div>);
};
export default Introduce;