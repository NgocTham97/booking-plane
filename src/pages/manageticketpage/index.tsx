import React, { useCallback, useEffect, useState } from "react";
import { InputForm } from "src/components/form";
import axiosClient from "src/config/call-api";
import Modal from "antd/lib/modal/Modal";
import SidebarLeft from "src/layout/SidebarLeft";
import { Table } from "antd";
import noData from '../../../src/assets/images/icon/No-data.svg';
import "./style/index.scss";
import del from '../../../src/assets/images/icon/delete-table.svg';
import edit from '../../../src/assets/images/icon/create-table.svg';
import see from '../../../src/assets/images/icon/see.svg';
import Text from 'antd/lib/typography/Text';
import moment, { Moment } from "moment";
import { Button } from "antd/lib/radio";

interface SearchedFlights {
    originCity: any;
    destinationCity: any;
    flights: any;
    flightStatus: any;
    length: any;
    departureTime: any;

}

const ManageTicket: React.FC = () => {
    const [data, setData] = useState<SearchedFlights>();
    const [id, setID] = useState();
    const getData = (id: any) => {
        axiosClient
            .get("/api/v1/flights/selected-tickets?flightId=" + id)
            .then((response) => {
                if (response && response.data) {
                    setData(response.data);
                }
            })
            .catch(() => { });
    };
    const getInputValue = (event: any) => {

        const id = event.target.value;
        setID(id);
    };
    const columns: any = [

        {
            title: 'CODEBOOKING',
            dataIndex: 'bookingId',
            render: (record: any) => <Text>{record}</Text>
        },
        {
            title: 'Daystart',
            dataIndex: 'departureTime',
            render: (record: any) => <Text>{moment(record).format("DD/MM/YYYY")}</Text>

        },
        {
            title: 'Name',
            dataIndex:'middleAndFirstName',
            width: '20%',
            render: (record: any) => <Text>{record}</Text>
        },
        {
            title: 'Gender',
            dataIndex: 'gender',
            render: (record: any) =><Text>{record}</Text>
        },
        {
            title: 'DateOfBirth',
            dataIndex: 'dateOfBirth',
            render: (record: any) => <Text>{moment(record).format("DD/MM/YYYY")}</Text>

        },
        {
            title: 'TicketType',
            dataIndex: 'ticketType',

        },
        {
            title: 'SeatCode',
            dataIndex: 'seatCode',

        },
    ];
    console.log('data', data)
    return (
        <div className="container-fluid page-body-wrapper body-wrapper">
            <div className="box-manage-flight">
                <div className="form-ticket">
                    <SidebarLeft />
                    <div className="form-manage-ticket">
                        <div className="box-form-code">
                            <div>Nhập ID Flight : </div>
                            <input onChange={getInputValue}></input>
                            <Button onClick={() => getData(id)
                            } >Nhập</Button>
                        </div>
                        <Table 
                            style={{width: '100%' , marginTop: '2rem'}}
                            dataSource={data as any}
                            columns={columns}
                            locale={{
                                emptyText: (
                                    <span>
                                        <img src={noData} alt='' />
                                    </span>
                                ),
                            }}
                        />
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ManageTicket;
