import React from "react";
import "./style/index.scss";
import { Input, Space } from 'antd';
import { AudioOutlined } from '@ant-design/icons';
import { Pagination } from 'antd';
const { Search } = Input;


const News: React.FC = () => {
    const { Search } = Input;
    const onSearch = (value: string) => console.log(value);
    const suffix = (
        <AudioOutlined
            style={{
                fontSize: 16,
                color: '#1890ff',
            }}
        />
    );
    return (
        <div className="container-fluid page-body-wrapper body-wrapper">
            <div className="container">
                <h1 className="title">News</h1>

                <div className="searchs">
                    <Search placeholder="input search text" onSearch={onSearch} enterButton />
                </div>
            <div className="box-form-news">
               <h2 className="content-news">Tin trong ngày</h2>
               <div className="form-news">
                   <div className="new">Tin số 1</div>
                   <div className="new">Tin số 1</div>
                   <div className="new">Tin số 1</div>
                   <div className="new">Tin số 1</div>
                   <div className="new">Tin số 1</div>
                   <div className="new">Tin số 1</div>
                   <div className="new">Tin số 1</div>
                   <div className="new">Tin số 1</div>
                   <div className="new">Tin số 1</div>

               </div>
               <Pagination style={{marginTop: '60px',marginBottom : '60px'}} defaultCurrent={1} total={50} />
            </div>

            </div>
        </div >
    );
};

export default News;
