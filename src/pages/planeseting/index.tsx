/* eslint-disable @typescript-eslint/no-unused-expressions */
import React , { useEffect, useState, useCallback } from "react";
import "./style/index.scss";
import { Link, useLocation } from "react-router-dom";
import { useHistory } from "react-router-dom";
import axiosClient from "src/config/call-api";
import { isTemplateExpression, isTypeAliasDeclaration } from "typescript";

interface SearchedFlights {
  id: any;
  seatCode: any;

}

const PlanSeting: React.FC = () => {
  const [data, setData] = useState<SearchedFlights[] | []>([]);
  const history = useHistory();
  const [isLoading, setIsLoading] = useState(false);
  const [currentSeatSelected, setCurrentSeatSelected] = useState<string[]>([]);
  const [selectSeat, setSelectSeat] = useState([]);
  const search = window.location.search;
  // const [id, setID] = useState(9);
  const params = new URLSearchParams(search);
  const id = params.get('id');

  useEffect(() => {
    setCurrentSeatSelected((data).map((data : any) => {return data?.seatCode} ))
  }, [data])

  useEffect(() => {
    localStorage.setItem("selected-seat", selectSeat as any)
    localStorage.setItem("search", search as any)
  }, [selectSeat, search])


  const a = [
    {
      row: [
        {
          seat: "1A",
        },
        {
          seat: "1B",
        },
        {
          seat: "1C",
        },
        {
          seat: "1D",
        },
        {
          seat: "1E",
        },
        {
          seat: "1F",
        },
      ],
    },
    {
      row: [
        {
          seat: "2A",
        },
        {
          seat: "2B",
        },
        {
          seat: "2C",
        },
        {
          seat: "2D",
        },
        {
          seat: "2E",
        },
        {
          seat: "2F",
        },
      ],
    },
    {
      row: [
        {
          seat: "3A",
        },
        {
          seat: "3B",
        },
        {
          seat: "3C",
        },
        {
          seat: "3D",
        },
        {
          seat: "3E",
        },
        {
          seat: "3F",
        },
      ],
    },
    {
      row: [
        {
          seat: "4A",
        },
        {
          seat: "4B",
        },
        {
          seat: "4C",
        },
        {
          seat: "4D",
        },
        {
          seat: "4E",
        },
        {
          seat: "4F",
        },
      ],
    },
    {
      row: [
        {
          seat: "5A",
        },
        {
          seat: "5B",
        },
        {
          seat: "5C",
        },
        {
          seat: "5D",
        },
        {
          seat: "5E",
        },
        {
          seat: "5F",
        },
      ],
    },
    {
      row: [
        {
          seat: "6A",
        },
        {
          seat: "6B",
        },
        {
          seat: "6C",
        },
        {
          seat: "6D",
        },
        {
          seat: "6E",
        },
        {
          seat: "6F",
        },
      ],
    },
    {
      row: [
        {
          seat: "7A",
        },
        {
          seat: "7B",
        },
        {
          seat: "7C",
        },
        {
          seat: "7D",
        },
        {
          seat: "7E",
        },
        {
          seat: "7F",
        },
      ],
    },
    {
      row: [
        {
          seat: "8A",
        },
        {
          seat: "8B",
        },
        {
          seat: "8C",
        },
        {
          seat: "8D",
        },
        {
          seat: "8E",
        },
        {
          seat: "8F",
        },
      ],
    },
  ];

  const getData = (id: any) => {
    axiosClient
      .get("/api/v1/flights/selected-tickets?flightId=" + id)
      .then((response) => {
        if (response && response.data) {
          setData(response.data);
        }
      })
      .catch(() => { });
  };
  useEffect(() => {
    getData(id);

  }, [id]);

  const handleSelectSeat = useCallback(item => {
    const hasSelect = selectSeat.findIndex(seat => seat === item)
    if(hasSelect !== -1) {
      const newArray: any = selectSeat.filter(seat => seat !== item)
      setSelectSeat(newArray)
    } else {
      const newArray: any = [...selectSeat, item]
      setSelectSeat(newArray)
    }
  }, [selectSeat])
  
  return (
    <div className="container-fluid page-body-wrapper body-wrapper">
      <div className="container">
        <div className="plane">
          <div className="cockpit">
            <h1>DRUA Project Seat Selection</h1>
          </div>
          <div className="exit exit--front fuselage"></div>
          <ol className="cabin fuselage">
            {a.map((item: any) => (
              <li
                className="row row--1"
                style={{ display: "block", margin: "0" }}
              >
                <ol className="seats" type="A">
                {item.row.map((seat: any) => (
                  <li className="seat">
                    <input
                     disabled={
                         currentSeatSelected.includes(seat.seat)
                        }
                         type="checkbox" id={seat.seat} />
                    <label htmlFor={seat.seat} onClick={() => handleSelectSeat(seat.seat)
                    }>{seat.seat}</label>
                  </li>
                ))}
                </ol>
              </li>
            ))}
          </ol>
          <div className="exit exit--back fuselage"></div>
        </div>
        <div className="under-plane">
          <button className="bnt-continue">
            <Link
              className="bnt-reservations"
              to={{
                pathname: "/pagepayment",

              }}
            >
              Tiếp tục
            </Link>
          </button>
        </div>
      </div>
    </div>
  );
};

export default PlanSeting;
