import React, { useCallback, useEffect, useState } from "react";
import { InputForm } from "src/components/form";
import axiosClient from "src/config/call-api";
import Modal from "antd/lib/modal/Modal";
import "./style/index.scss";
import moment from "moment";
import { Button } from "antd/lib/radio";
interface SearchedFlights {
  id: any
  seatCode: any
  booking: any
  middleAndFirstName: any
  selectedTickets: any
  flightNumber: any
  aircraftType: any
  cityName:any
}
const ReviewOrder: React.FC = () => {
  const [id, setID] = useState();
  const [data, setData] = useState<SearchedFlights>();
  const [detail, setDetail] = useState(false);
  const [detailflight, setDetailFlight] = useState<SearchedFlights>();
  const [getCity, setGetCity] = useState<SearchedFlights[] | []>([]);
  const [isLoading, setIsLoading] = useState(false);
  const fetchgetCity = useCallback(() => {
    setIsLoading(true);
    axiosClient
      .get(`/api/v1/cities`)
      .then((response) => {
        if (response && response.data) {
          setGetCity(response.data);
        }
        setIsLoading(false);
      })
      .catch((e) => {
        setIsLoading(false);
      });
  }, []);
  const getFlight = (id: any) => {
    axiosClient
      .get("/api/v1/flights/" + id)
      .then((response) => {
        if (response && response.data) {
          setDetailFlight(response.data);
        }
      })
      .catch(() => { });
  };
  const getData = (id: any) => {
    axiosClient
      .get("/api/v1/bookings/" + id)
      .then(async (response) => {
        if (response && response.data) {
          setData(response.data);
          await getFlight(response.data?.selectedTickets[0]?.flightId)
          await fetchgetCity()
          setDetail(true);
        }
      })
      .catch(() => { });
  };
  const getInputValue = (event: any) => {

    const id = event.target.value;
    setID(id);
  };
  const dressShow = (id: any) => {
    const curentCity = (getCity).find((city) => city.id === id)

    console.log('curentCity', curentCity)
    console.log('getCity', getCity)

    return curentCity ? curentCity?.cityName : ''


  }
  return (
    <div className="container-fluid page-body-wrapper body-wrapper">
      <div className="container">
        <div className="box-form-code">
          <div>Nhập vào mã code của bạn</div>
          <input onChange={getInputValue}></input>
          <Button  onClick={() => getData(id)
          } >Nhập</Button>
        </div>
        {detail === true ? (

          <Modal
            style={{ marginTop: '6rem' }}
            onCancel={() => setDetail(false)}
            visible={detail}
            title="Thông tin vé của bạn là"
            cancelButtonProps={{ style: { display: 'none' } } as any}
            onOk={() => setDetail(false)}
          >
            <div>
              <div className="time-booking">
                <div className="title">Thời gian đặt vé:</div>
                <div>{moment(data?.booking?.bookingTime).format("hh:mm")}</div>
                <div>{moment(data?.booking?.bookingTime).format("DD/MM/YYYY")}</div>

              </div>
              <div className="phone-booking">
                <div className="title">Số điện thoại đăng ký :</div>
                <div>{data?.booking?.contactPhoneNumber}</div>
              </div>
              <div className="phone-booking">
                <div className="title">Gmail đăng ký :</div>
                <div>{data?.booking?.contactEmail}</div>
              </div>
              <div className="seat-booking">
                <div className="title">Vị trí ghế đã đặt:</div>
                <div>{data?.selectedTickets[0]?.seatCode}</div>
              </div>
              <div className="pre-name-booking">
                <div className="title">Họ và tên :</div>
                <div>{data?.selectedTickets[0]?.familyName}</div>
                <div>{data?.selectedTickets[0]?.middleAndFirstName}</div>
               
              </div>
              <div className="date-booking">
                <div className="title">Ngày sinh</div>
                <div>{moment(data?.selectedTickets[0]?.dateOfBirth).format("DD/MM/YYYY")}</div>
              </div>
              <div className="date-booking">
                <div className="title">Giới tính :</div>
                <div>{data?.selectedTickets[0]?.gender === 'Male' ? 'Nam' : 'Nữ'}</div>
              </div>
              <div className="information">
                <div className="title">Chuyến bay số:</div>
                <div>{detailflight?.flightNumber}</div>
              </div>
              <div className="type-air">
                <div className="title">Loại máy bay:</div>
                <div>{detailflight?.aircraftType}</div>
              </div>
              <div className="type-air">
                <div className="title">Điểm đi</div>
                <div>{dressShow((detailflight as any)?.originAirportId)}</div>
              </div>
              <div className="type-air">
                <div className="title">Điểm đến</div>
                <div>{dressShow((detailflight as any)?.destinationAirportId)}</div>

              </div>
            </div>

          </Modal>
        ) : ''}


      </div>
    </div>
  );
};

export default ReviewOrder;
