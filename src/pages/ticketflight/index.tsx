import React, { useEffect, useState, useCallback } from "react";
import "./style/index.scss";
import logoplan from "../../assets/images/images.png";
import logo from "../../assets/images/arrow-next.png";
import plan from "../../assets/images/304892.svg";
import { Link, useLocation } from "react-router-dom";
import check from "../../assets/images/299110_check_sign_icon.png";
import { useHistory } from "react-router-dom";
import axiosClient from "src/config/call-api";
import moment from "moment";
interface SearchedFlights {
  id: any;
  departureTime: any;
  rootPrice: any;
  flightNumber: any;
  aircraftType: any;
  arrivalTime: any;
  shortCode: any;
  cityName: any;
}
const TicketFlight: React.FC = () => {
  const history = useHistory();
  const [getCity, setGetCity] = useState<SearchedFlights[] | []>([]);
  const [isLoading, setIsLoading] = useState(false);
  const [data, setData] = useState<SearchedFlights>();
  const search = window.location.search;
  const params = new URLSearchParams(search);
  const [detail, setDetail] = useState(false);
  const id = params.get('id');
  useEffect(() => {
    getData(id);

  }, [id]);

  const handledetail = () => {
    if (detail === false) { setDetail(true); }
    else
      return setDetail(false)
  }
  const fetchgetCity = useCallback(() => {
    setIsLoading(true);
    axiosClient
      .get(`/api/v1/cities`)
      .then((response) => {
        if (response && response.data) {
          setGetCity(response.data);
        }
        setIsLoading(false);
      })
      .catch((e) => {
        setIsLoading(false);
      });
  }, []);
  useEffect(() => {

    fetchgetCity();
  }, [fetchgetCity]);
  const getData = (id: any) => {
    axiosClient
      .get("/api/v1/flights/" + id)
      .then((response) => {
        if (response && response.data) {
          setData(response.data);
        }
      })
      .catch(() => { });
  };

  const dressShow = (id: any) => {
    const curentCity = (getCity).find((city) => city.id === id)

    console.log('curentCity', curentCity)
    console.log('getCity', getCity)

    return curentCity ? curentCity?.cityName : ''


  }
  return (
    <div className="container-fluid page-body-wrapper body-wrapper">
      <div className="container">
        <div className="nav-all">
          <div className="title-ticket-flight">
            Xem lại lựa chọn chuyến bay của Quý khách
          </div>
          <div className="nav-ticket-flight">
            <div className="nav-left">
              <div className="note-box">
                <div className="title">
                  Tra cứu thông tin hành lý<a href="">tại đây</a>
                </div>
                <div style={{ color: "red" }} className="title">
                  Lưu ý: Giá dưới đây đã bao gồm thuế, phí
                </div>
                <div
                  style={{ color: "blue", cursor: "pointer" }}
                  className="title"
                >
                  + Phí Dịch Vụ Đặc Biệt
                </div>
                <div
                  style={{ color: "blue", cursor: "pointer" }}
                  className="title"
                >
                  + Thuế, Phí, Lệ phí & Phụ thu
                </div>
                <div className="title">
                  Đồng tiền thanh toán hiển thị theo “Quốc gia/Vùng” đã chọn,
                  Quý khách kiểm tra kỹ đồng tiền trước khi thanh toán.
                </div>
              </div>

              <div className="box-content-section-ticket">
                <div className="box-content-section-ticket-top">
                  <div className="date-start">
                    <div className="date-time-box">
                      <img
                        src={check}
                        style={{ width: "30px", height: "30px" }}
                        alt=""
                      />
                      <div className="content">Khởi hành</div>
                      <div className="time">{moment(data?.departureTime).format("DD/MM/YYYY")}</div>
                    </div>
                  </div>
                  <button
                    onClick={() => {
                      history.goBack();
                    }}
                    className="change"
                  >
                    Thay đổi
                  </button>
                </div>

                <div className="box-content-section-ticket-under">
                  <div className="box-content-section-left">
                    <div className="journeys">
                      <div className="go">
                        <div className="time-go"> {moment(data?.departureTime).format("HH:mm")}</div>
                        <div className="dress">{
                          dressShow((data as any)?.originAirportId)
                        }</div>
                      </div>

                      <div className="desnitation">
                        <div className="time-go"> {moment(data?.arrivalTime).format("HH:mm")}</div>
                        <div className="dress"> {dressShow((data as any)?.destinationAirportId)}</div>
                      </div>
                    </div>
                    <div className="time-all">
                      {/* <div className="time-a">1 tiếng 25 phút</div>{" "} */}
                      <p>Bay Thẳng</p>{" "}
                    </div>
                    <div onClick={() => handledetail()}
                      className="detail-button"> Xem chi tiết </div>
                  </div>
                  <div className="box-content-section-center">
                    <div className="name-plane">VN{data?.flightNumber}</div>
                    <div className="name-plane">{data?.aircraftType}</div>
                  </div>
                  <div className="box-content-section-right"></div>
                </div>
              </div>
              {
                detail &&
                (
                  <div className="detail">
                    <div className="content-top-detail">
                      <div className="date">{moment(data?.departureTime).format("DD/MM/YYYY")},</div>
                      <div className="time">{moment(data?.departureTime).format("HH:mm")}</div>
                      <div className="city">
                        {
                          dressShow((data as any)?.originAirportId)
                        }
                      </div>
                    </div>
                    <div className="airlinecompany">
                      <div className="air"> Chuyến bay:</div>
                      <div> VN{data?.flightNumber}</div>
                    </div>
                    <div className="airtype">
                      <div className="air">Máy Bay:</div>
                      <div> {data?.aircraftType}</div>

                    </div>
                    {/* <div className="time-go">

                      <div className="air">  Thời gian di chuyển :</div>
                      <div></div>
                    </div> */}
                    <div className="content-top-detail">
                      <div className="time">{moment(data?.arrivalTime).format("HH:mm")}</div>
                      <div className="city">
                        {dressShow((data as any)?.destinationAirportId)}
                      </div>
                    </div>
                  </div>)
              }
              <button className="bnt-continue">
                <Link
                  className="bnt-reservations"
                  to={{
                    pathname: "/information",
                    search: `?id=${data?.id}`,
                  }}
                >
                  Tiếp tục
                </Link>
              </button>
            </div>

            <div className="nav-right">
              <div className="note-box-right">
                <div className="box-content">
                  <p className="title-content">Chi tiết chuyến bay</p>
                </div>
                <div className="flight-section">
                  <div className="flight-section-left">
                    <img
                      src={logoplan}
                      style={{ width: "30px", height: "30px" }}
                      alt=""
                    />

                    <div className="city">HAN</div>
                    <img
                      src={logo}
                      style={{ width: "30px", height: "15px" }}
                      alt=""
                    />
                    <div className="city">DAD</div>
                  </div>
                  <div className="flight-section-right">{data?.rootPrice * 23000}VND </div>
                </div>
                <div className="flight-section-note">
                  <div className="time-start">
                    <div className="title">Khởi hành</div>
                    <div> {moment(data?.departureTime).format("HH:mm")} </div>
                    <div>{moment(data?.departureTime)

                      .format("DD/MM/YYYY")}</div>
                  </div>
                  <div className="flight-section-detail">
                    <div className="flight">
                      <div> {dressShow((data as any)?.originAirportId)}</div>
                      <img
                        src={plan}
                        style={{ width: "15px", height: "15px" }}
                        alt=""
                      />
                      <div> {dressShow((data as any)?.destinationAirportId)}</div>
                    </div>
                    {/* <div className="time">
                      <div>Thời gian:</div>
                      <div>1 tiếng 25 phút</div>
                    </div> */}
                    <div className="plane">
                      <div>VN{data?.flightNumber}</div>
                      <div>{data?.aircraftType}</div>
                    </div>
                  </div>
                  <div className="person">
                    <div className="title">Người lớn x 1 </div>
                    <div className="title">{data?.rootPrice * 23000}VND</div>
                  </div>
                </div>
                <div className="price">
                  <div className="price-all">Tổng số tiền</div>
                  <div className="price-in">{data?.rootPrice * 23000}VND</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TicketFlight;
