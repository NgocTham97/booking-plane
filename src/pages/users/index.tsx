import React, { useCallback, useEffect, useState } from "react";
import SidebarLeft from "src/layout/SidebarLeft";
import { Table } from "antd";
import { StripedTable } from "src/components/table";
import axiosClient from "src/config/call-api";
import avatarNotFound from "../../assets/images/faces/avatar-not-found.png";
import Text from 'antd/lib/typography/Text';
import moment, { Moment } from "moment";
import del from '../../../src/assets/images/icon/delete-table.svg';
import edit from '../../../src/assets/images/icon/create-table.svg';
import noData from '../../../src/assets/images/icon/No-data.svg';
import { Button } from 'antd';
import "./style/index.scss";
import addbutton from '../../assets/images/icon/add_circle_outline.svg'


interface SearchedFlights {
  originCity: any;
  destinationCity: any;
  flights: any;
  flightStatus: any;
  length: any;
  departureTime: any;

}
const Users: React.FC = () => {
  const [data, setData] = useState<SearchedFlights>();
  const [getCity, setGetCity] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [perPage, setPerPage] = useState(1);
  const [page, setPage] = useState(1);
  const [delLs, setDelLs] = useState(false);
  const [importFlight, setImportFlight] = useState(false);
  const [importFlightNote, setImportFlightNote] = useState(false);
  const [delLsNote, setDelLsNote] = useState(false);
  const [id, setId] = useState(null);
  const dataSource = [];
  const [destinationCities, setDestinationCities] = useState([]);
  const userArray = [
    {
   
    name: 'Mike',
    idname: 'NguyenHoang',
    numbersdt: '0787702293',
    mail: 'Ngoctham12a1@gmail.com',
    nation:'Việt Nam'
  },
  {
  
    name: 'Mike',
    idname: 'NguyenHoang',
    numbersdt: '0787702293',
    mail: 'Ngoctham12a1@gmail.com',
    nation:'Việt Nam'
  },
  {
   
    name: 'Mike',
    idname: 'NguyenHoang',
    numbersdt: '0787702293',
    mail: 'Ngoctham12a1@gmail.com',
    nation:'Việt Nam'
  },
  {
   
    name: 'Mike',
    idname: 'NguyenHoang',
    numbersdt: '0787702293',
    mail: 'Ngoctham12a1@gmail.com',
    nation:'Việt Nam'
  },

];
 
  const columns: any = [

    {
      title: 'Họ và tên',
      dataIndex: 'name',

    },
    {
      title: 'Tên đăng nhập',
      dataIndex: 'idname',
    

    },
    {
      title: 'SĐT',
      dataIndex: 'numbersdt',
    
    },
    {
      title: 'Mail',
      dataIndex: 'mail',
     
    },
    {
      title: 'Quốc gia',
      dataIndex: 'nation',
    

    },
    {
      title: 'Thao tác',
      width: '10%',
      align: 'left',
      render: (_: any, record: any) => (
        <div className="wrapper-action">
          {/* <div className="wrap-icon">
                  <img src={see} alt='' />
                </div> */}
          <div className="wrap-icon">
            <img src={edit} alt='' />
          </div>
          <div className="wrap-icon">
            <img onClick={() => {
              setDelLs(true);
              setId(record.id);

            }} src={del} alt='' />
          </div>
        </div>
      ),
    },
  ];
  return (
    <div className="container-fluid page-body-wrapper body-wrapper">

      <SidebarLeft />
      <div className="box-form-code">
        <h1 className="title">Quản lý Users</h1>

        <Table
          dataSource={userArray as any}
          columns={columns}
          locale={{
            emptyText: (
              <span>
                <img src={noData} alt='' />
              </span>
            ),
          }}
        />
         <Button
          style={{marginBottom: '24px'}}
           onClick={() => { setImportFlight(true) }} >
          <img className="button_add" src={addbutton} alt="" />
            Thêm</Button>
      </div>
    </div>


  );
};

export default Users;
