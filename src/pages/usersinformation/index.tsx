import React, { useCallback, useEffect, useState } from "react";
import SidebarLeft from "src/layout/SidebarLeft";
import { Table } from "antd";
import { StripedTable } from "src/components/table";
import axiosClient from "src/config/call-api";
import avatarNotFound from "../../assets/images/faces/avatar-not-found.png";
import { Link } from "react-router-dom";
import Text from 'antd/lib/typography/Text';
import moment, { Moment } from "moment";
import del from '../../../src/assets/images/icon/delete-table.svg';
import edit from '../../../src/assets/images/icon/create-table.svg';
import noData from '../../../src/assets/images/icon/No-data.svg';
import { Button } from 'antd';
import "./style/index.scss";
import addbutton from '../../assets/images/icon/add_circle_outline.svg'


interface SearchedFlights {
    originCity: any;
    destinationCity: any;
    flights: any;
    flightStatus: any;
    length: any;
    departureTime: any;

}
const UsersInformation: React.FC = () => {
    const [data, setData] = useState<SearchedFlights>();
    const [getCity, setGetCity] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [perPage, setPerPage] = useState(1);
    const [page, setPage] = useState(1);
    const [delLs, setDelLs] = useState(false);
    const [importFlight, setImportFlight] = useState(false);
    const [importFlightNote, setImportFlightNote] = useState(false);
    const [delLsNote, setDelLsNote] = useState(false);
    const [id, setId] = useState(null);
    const dataSource = [];
    const [destinationCities, setDestinationCities] = useState([]);
    const userArray = [
        {

            name: '11312',
            idname: '200681',
            numbersdt: 'VN45',
            mail: '12/05/2022',
            nation: '12/05/2022',
            nationone:'HNI',
            nationtwo:'SGN',
            tickettype:'Vé phổ thông',
            seatcode:'5F'
        },
    ];

    const columns: any = [

        {
            title: 'Mã vé',
            dataIndex: 'name',

        },
        {
            title: 'Mã đặt vé',
            dataIndex: 'idname',


        },
        {
            title: 'Mã chuyến bay',
            dataIndex: 'numbersdt',

        },
        {
            title: 'Thời gian đi',
            dataIndex: 'mail',

        },
        {
            title: 'Thời gian đến',
            dataIndex: 'nation',


        },
        {
            title: 'Địa điểm đi',
            dataIndex: 'nationone',


        },
        {
            title: 'Địa điểm đến',
            dataIndex: 'nationtwo',


        },
        {
            title: 'Loại vé',
            dataIndex: 'tickettype',


        },
        {
            title: 'Ghế ngồi',
            dataIndex: 'seatcode',
        },

    ];
    return (
        <div className="container-fluid page-body-wrapper body-wrapper">

            <div className="sibar-left">
                <nav className="sidebar sidebar-offcanvas" id="sidebar">
                    <ul className="nav">
                        <li className="nav-item">
                            <Link
                                className="nav-link"
                                to={{
                                    //   pathname: "/admin",
                                }}
                            >
                                <i className="ti-write menu-icon"></i>

                                <span className="menu-title">Quản lý đặt vé</span>
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link
                                className="nav-link"
                                to={{
                                    // pathname: "/manageticketpage",
                                }}
                            >
                                <i className="ti-write menu-icon"></i>

                                <span className="menu-title">Hoàn vé</span>
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link
                                className="nav-link"
                                to={{
                                    pathname: "/users",
                                }}
                            >
                                <i className="ti-write menu-icon"></i>

                                <span className="menu-title">Đổi vé</span>
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link to="#" className="nav-link">
                                <i className="ti-write menu-icon"></i>

                                <span className="menu-title">Xuất thông tin vé</span>
                            </Link>
                        </li>
                    </ul>
                </nav>
            </div>
            <div className="box-form-code"> 
                <Table
                    dataSource={userArray as any}
                    columns={columns}
                    locale={{
                        emptyText: (
                            <span>
                                <img src={noData} alt='' />
                            </span>
                        ),
                    }}
                />
                <div className="button-users">
                    <Button
                        style={{ marginBottom: '24px' }}
                        onClick={() => { setImportFlight(true) }} >

                        Hoàn vé</Button>
                        <Button
                        style={{ marginBottom: '24px' }}
                        onClick={() => { setImportFlight(true) }} >

                        Đổi vé</Button>
                        <Button
                        style={{ marginBottom: '24px' }}
                        onClick={() => { setImportFlight(true) }} >

                        Xuất thông tin</Button>
                </div>
            </div>
        </div>


    );
};

export default UsersInformation;
